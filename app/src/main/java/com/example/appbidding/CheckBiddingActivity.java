package com.example.appbidding;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CheckBiddingActivity extends AppCompatActivity {
    TextView no_rekening, atas_nama;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_bidding);

        no_rekening = (TextView)findViewById(R.id.no_rekening);
        atas_nama = (TextView)findViewById(R.id.atas_nama);
        SpannableString spannablecontent=new SpannableString("No. Rek: 0266258538");
        spannablecontent.setSpan(new StyleSpan(Typeface.BOLD),
                9,spannablecontent.length(), 0);
        no_rekening.setText(spannablecontent);

        SpannableString spannablecontent2=new SpannableString("a/n Daya Gagas Perikanan");
        spannablecontent2.setSpan(new StyleSpan(Typeface.BOLD),
                4,24, 0);
        atas_nama.setText(spannablecontent2);

        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBiddingActivity.super.onBackPressed();
            }
        });
    }
}
