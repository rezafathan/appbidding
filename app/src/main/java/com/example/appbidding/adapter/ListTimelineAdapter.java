package com.example.appbidding.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.appbidding.HomeActivity;
import com.example.appbidding.R;
import com.example.appbidding.fragment.DetailFailedFragment;
import com.example.appbidding.fragment.DetailFragment;
import com.example.appbidding.model.ListBidding;
import com.example.appbidding.model.ListTimeline;

import java.util.ArrayList;

public class ListTimelineAdapter extends RecyclerView.Adapter<ListTimelineAdapter.ListTimelineViewHolder>{
    private ArrayList<ListTimeline> arrayList;
    Context context;

    public ListTimelineAdapter(ArrayList<ListTimeline> arrayList, Context context){
        this.arrayList = arrayList;
        this.context =context;
    }

    @Override
    public ListTimelineViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.card_timeline, parent, false);
        return new ListTimelineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListTimelineViewHolder holder, int position){
        holder.time.setText(arrayList.get(position).getTime());
        holder.value.setText(arrayList.get(position).getValue());
        holder.company.setText(arrayList.get(position).getCompany());
        holder.status_view.setImageResource(arrayList.get(position).getStatus_view());
    }

    @Override
    public int getItemCount(){
        return (arrayList != null ? arrayList.size() : 0);
    }
    public class ListTimelineViewHolder extends RecyclerView.ViewHolder{
        private TextView time, value, company;
        ImageView status_view;

        public ListTimelineViewHolder(View itemView){
            super(itemView);
            time = (TextView)itemView.findViewById(R.id.time);
            value = (TextView)itemView.findViewById(R.id.value);
            company = (TextView)itemView.findViewById(R.id.company);
            status_view = (ImageView)itemView.findViewById(R.id.status_view);
        }
    }
}
