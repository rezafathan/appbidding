package com.example.appbidding.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.appbidding.CheckBiddingActivity;
import com.example.appbidding.HomeActivity;
import com.example.appbidding.R;
import com.example.appbidding.fragment.BiddingFragment;
import com.example.appbidding.fragment.DetailFailedFragment;
import com.example.appbidding.fragment.DetailFragment;
import com.example.appbidding.model.ListBidding;

import java.util.ArrayList;

public class ListBiddingAdapter extends RecyclerView.Adapter<ListBiddingAdapter.ListBiddingViewHolder> {
    private ArrayList<ListBidding> arrayList;
    Context context;
    public ListBiddingAdapter(ArrayList<ListBidding> arrayList, Context context){
        this.arrayList = arrayList;
        this.context =context;
    }

    @Override
    public ListBiddingViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.card_list_bidding, parent, false);
        return new ListBiddingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListBiddingViewHolder holder, int position){
        holder.id.setText(arrayList.get(position).getId());
        holder.date.setText(arrayList.get(position).getDate());
        holder.time.setText(arrayList.get(position).getTime());
        holder.fish.setText(arrayList.get(position).getFish());
        holder.weight.setText(arrayList.get(position).getWeight());
        holder.bidding.setText(arrayList.get(position).getBidding());
        // 1 Expired, 0 normal, 2 bidding saya
        if (arrayList.get(position).getStatus()==1){
            holder.card_box.setBackgroundColor(Color.parseColor("#FDD5D5"));
            holder.detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment fragment = new DetailFailedFragment();
                    FragmentManager fragmentManager = ((HomeActivity)context).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });
        }
        else if(arrayList.get(position).getStatus()==2){
            holder.card_box.setBackgroundColor(Color.parseColor("#FFFFFF"));
            holder.detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CheckBiddingActivity.class);
                    context.startActivity(intent);
                }
            });
        }
        else{
            holder.card_box.setBackgroundColor(Color.parseColor("#FFFFFF"));
            holder.detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment fragment = new DetailFragment();
                    FragmentManager fragmentManager = ((HomeActivity)context).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });
        }

    }

    @Override
    public int getItemCount(){
        return (arrayList != null ? arrayList.size() : 0);
    }
    public class ListBiddingViewHolder extends RecyclerView.ViewHolder{
        private TextView id, date, time, fish, weight, bidding;
        Button detail;
        LinearLayout card_box;

        public ListBiddingViewHolder(View itemView){
            super(itemView);
            id = (TextView)itemView.findViewById(R.id.id);
            date = (TextView)itemView.findViewById(R.id.date);
            time = (TextView)itemView.findViewById(R.id.time);
            fish = (TextView)itemView.findViewById(R.id.fish);
            weight = (TextView)itemView.findViewById(R.id.weight);
            bidding = (TextView)itemView.findViewById(R.id.bidding);

            card_box = (LinearLayout)itemView.findViewById(R.id.card_box);
            detail = (Button)itemView.findViewById(R.id.detail);
        }
    }
}
