package com.example.appbidding.model;

public class ListTimeline {
    String time;
    String value;
    String company;
    Integer status_view;

    public ListTimeline(String time, String value, String company, Integer status_view) {
        this.time = time;
        this.value = value;
        this.company = company;
        this.status_view = status_view;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Integer getStatus_view() {
        return status_view;
    }

    public void setStatus_view(Integer status_view) {
        this.status_view = status_view;
    }
}
