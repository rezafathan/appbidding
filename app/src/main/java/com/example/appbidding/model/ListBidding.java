package com.example.appbidding.model;

public class ListBidding {
    String id;
    String date;
    String time;
    String fish;
    String weight;
    String bidding;
    Integer status;

    public ListBidding (String id, String date, String time, String fish, String weight, String bidding, Integer status){
        this.id = id;
        this.date = date;
        this.time = time;
        this.fish = fish;
        this.weight = weight;
        this.bidding = bidding;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFish() {
        return fish;
    }

    public void setFish(String fish) {
        this.fish = fish;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBidding() {
        return bidding;
    }

    public void setBidding(String bidding) {
        this.bidding = bidding;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
