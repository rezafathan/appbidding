package com.example.appbidding.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.appbidding.R;
import com.example.appbidding.adapter.ListTimelineAdapter;
import com.example.appbidding.model.ListTimeline;

import java.util.ArrayList;

public class DetailFailedFragment extends Fragment {
    View view;
    ImageView back;
    private ArrayList<ListTimeline> listTimelineArrayList;
    RecyclerView recyclerView;
    ListTimelineAdapter listTimelineAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_detail_failed, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);
        back = (ImageView)view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        addData();
        listTimelineAdapter = new ListTimelineAdapter(listTimelineArrayList, getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listTimelineAdapter);
        return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void addData() {
        listTimelineArrayList = new ArrayList<>();
        listTimelineArrayList.add(new ListTimeline("1 hour ago","Rp 201.000.000","PT. Jaya Abadi", R.drawable.blue_circle_line));
        listTimelineArrayList.add(new ListTimeline("2 hour ago","Rp 200.000.000","PT. Asep Bachri", R.drawable.white_circle_line));
    }
}
