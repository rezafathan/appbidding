package com.example.appbidding.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.appbidding.R;
import com.example.appbidding.adapter.ListBiddingAdapter;
import com.example.appbidding.model.ListBidding;

import java.util.ArrayList;

public class BiddingFragment extends Fragment {
    View view;
    private ArrayList<ListBidding> listBiddingArrayList;
    RecyclerView recyclerView;
    ListBiddingAdapter listBiddingAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bidding, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);
        addData();
        listBiddingAdapter = new ListBiddingAdapter(listBiddingArrayList, getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listBiddingAdapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void addData(){
        listBiddingArrayList = new ArrayList<>();
        listBiddingArrayList.add(new ListBidding("ID 08098909","09/03/2019","","Ikan Tuna Merah","10 Ton","Rp 200.000.000",2));
        listBiddingArrayList.add(new ListBidding("ID 08098909","09/03/2019","","Ikan Tuna Merah","10 Ton","Rp 100.000.000",2));
    }
}
