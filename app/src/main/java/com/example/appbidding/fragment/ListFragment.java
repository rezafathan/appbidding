package com.example.appbidding.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.appbidding.R;
import com.example.appbidding.adapter.ListBiddingAdapter;
import com.example.appbidding.model.ListBidding;

import java.util.ArrayList;

public class ListFragment extends Fragment {
    View view;
    TabLayout tabLayout;
    private ArrayList<ListBidding> listBiddingArrayList, listBiddingArrayList2;
    RecyclerView recyclerView, recyclerView2;
    ListBiddingAdapter listBiddingAdapter, listBiddingAdapter2;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_1);
        recyclerView2 = (RecyclerView)view.findViewById(R.id.recycler_view_2);
        tabLayout = (TabLayout)view.findViewById(R.id.tablayout);
        tabLayout.addTab(tabLayout.newTab().setText("All Bidding"));
        tabLayout.addTab(tabLayout.newTab().setText("Your Bidding"));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition()==0){
                    recyclerView.setVisibility(View.VISIBLE);
                    recyclerView2.setVisibility(View.GONE);
                }
                else {
                    recyclerView.setVisibility(View.GONE);
                    recyclerView2.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        addData();
        listBiddingAdapter = new ListBiddingAdapter(listBiddingArrayList, getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listBiddingAdapter);

        listBiddingAdapter2 = new ListBiddingAdapter(listBiddingArrayList2, getActivity());
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        recyclerView2.setLayoutManager(layoutManager2);
        recyclerView2.setAdapter(listBiddingAdapter2);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void addData(){
        listBiddingArrayList = new ArrayList<>();
        listBiddingArrayList.add(new ListBidding("ID 08098909","09/03/2019","1 hour ago","Ikan Tuna Merah","10 Ton","Rp 200.000.000",0));
        listBiddingArrayList.add(new ListBidding("ID 08098909","09/03/2019","1 hour ago","Ikan Tuna Merah","10 Ton","Rp 100.000.000",0));
        listBiddingArrayList.add(new ListBidding("ID 08098909","09/03/2019","1 hour ago","Ikan Tuna Merah","10 Ton","Rp 300.000.000",0));

        listBiddingArrayList2 = new ArrayList<>();
        listBiddingArrayList2.add(new ListBidding("ID 08098909","09/03/2019","1 hour ago","Ikan Tuna Merah","10 Ton","Rp 200.000.000",1));
        listBiddingArrayList2.add(new ListBidding("ID 08098909","09/03/2019","1 hour ago","Ikan Tuna Merah","10 Ton","Rp 100.000.000",0));
        listBiddingArrayList2.add(new ListBidding("ID 08098909","09/03/2019","1 hour ago","Ikan Tuna Merah","10 Ton","Rp 300.000.000",0));

    }
}
