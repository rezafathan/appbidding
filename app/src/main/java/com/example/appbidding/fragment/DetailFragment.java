package com.example.appbidding.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.appbidding.R;
import com.example.appbidding.adapter.ListBiddingAdapter;
import com.example.appbidding.adapter.ListTimelineAdapter;
import com.example.appbidding.model.ListBidding;
import com.example.appbidding.model.ListTimeline;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class DetailFragment extends Fragment {
    View view;
    ImageView back;
    TextView value;
    private ArrayList<ListTimeline> listTimelineArrayList;
    RecyclerView recyclerView;
    ListTimelineAdapter listTimelineAdapter;
    Button plus, minus;
    int nominal = 200000000;
    DecimalFormat decimalFormat;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_detail, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);
        plus = (Button)view.findViewById(R.id.plus);
        minus = (Button)view.findViewById(R.id.minus);
        back = (ImageView)view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        decimalFormat = (DecimalFormat)DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        decimalFormat.setDecimalFormatSymbols(formatRp);
        value = (TextView)view.findViewById(R.id.value);
        value.setText(decimalFormat.format((double)nominal));
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value.setText(decimalFormat.format((double)nominal+100000));
                nominal = nominal+100000;
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value.setText(decimalFormat.format((double)nominal-100000));
                nominal = nominal-100000;
            }
        });



        addData();
        listTimelineAdapter = new ListTimelineAdapter(listTimelineArrayList, getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listTimelineAdapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void addData() {
        listTimelineArrayList = new ArrayList<>();
        listTimelineArrayList.add(new ListTimeline("1 hour ago","Rp 201.000.000","PT. Jaya Abadi", R.drawable.blue_circle_line));
        listTimelineArrayList.add(new ListTimeline("2 hour ago","Rp 200.000.000","PT. Asep Bachri", R.drawable.white_circle_line));
    }
}
